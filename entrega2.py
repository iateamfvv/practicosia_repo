import itertools
from simpleai.search import CspProblem, backtrack,min_conflicts, MOST_CONSTRAINED_VARIABLE, LEAST_CONSTRAINING_VALUE, HIGHEST_DEGREE_VARIABLE


#Manejando los x e y en tuplas dentro del diccionario
tuplas = [(x, y)for x in range(8) for y in range(8)]
variables = ["K", "Q", "A1", "A2", "C1", "C2", "T1", "T2"]
domains = dict((variable, tuplas[:])for variable in variables)


def diferente_columna(variables, valores):
    col1 = valores[0][1]
    col2 = valores[1][1]
    pieza1, pieza2 = [pieza for pieza in variables]
    if (pieza1 in ["Q", "T1", "T2"]) or (pieza2 in ["Q", "T1", "T2"]):
        return col1 != col2
    else:
        return True


def diferente_fila(variables, valores):
    fila0 = valores[0][0]
    fila1 = valores[1][0]
    pieza1, pieza2 = [reina for reina in variables]
    if (pieza1 in ["Q", "T1", "T2"]) or (pieza2 in ["Q", "T1", "T2"]):
        return fila0 != fila1
    else:
        return True


def no_diagonal(variables, valores):
    col1 = valores[0][1]
    col2 = valores[1][1]
    fila0 = valores[0][0]
    fila1 = valores[1][0]
    pieza1, pieza2 = [pieza for pieza in variables]
    if (pieza1 in ["Q", "A2", "A1"]) or (pieza2 in ["Q", "A2", "A1"]):
        resta_col = abs(col1 - col2)
        resta_fil = abs(fila0 - fila1)
        return resta_fil != resta_col
    else:
        return True


def rey(variables, valores):
    pieza1, pieza2 = [pieza for pieza in variables]
    col1 = valores[0][1]
    col2 = valores[1][1]
    fila0 = valores[0][0]
    fila1 = valores[1][0]
    if pieza1 or pieza2 == "K":
        if (abs(col2 - col1) > 1) or (abs(fila0 - fila1) > 1):
        #Si el rey no tiene piezas en las casillas adyacentes
            return True
        else:
            return False
    else:
        return True


def caballo(variables, valores):
    pieza1, pieza2 = [pieza for pieza in variables]
    if (pieza1 in ["C1", "C2"]) or (pieza2 in ["C1", "C2"]):
        col1 = valores[0][1]
        col2 = valores[1][1]
        fila0 = valores[0][0]
        fila1 = valores[1][0]
        resultado = (abs(fila1 - fila0), abs(col1 - col2))
        if (resultado == (2, 1)) or (resultado == (1, 2)):
            return False
        else:
            return True
    else:
        return True


restricciones = []


for par in itertools.combinations(variables, 2):
    restricciones.append((par, diferente_fila))
    restricciones.append((par, diferente_columna))
    restricciones.append((par, no_diagonal))
    restricciones.append((par, rey))
    restricciones.append((par, caballo))


def resolver(metodo_busqueda, heuristica_valor, heuristica_variable):
    csp = CspProblem(variables, domains, restricciones)
    #metodo = dicc["metodo_busqueda"]
    #heuristica_variable = dicc["heuristica_variable"]
    #heuristica_valor = dicc["heuristica_valor"]   
    resultado = {}
    if metodo_busqueda == "backtrack":
        if heuristica_variable == "None" and heuristica_valor == "None":
            resultado = backtrack(csp, inference=False)
        if heuristica_variable == "MOST_CONSTRAINED_VARIABLE" and heuristica_valor == "None":
            resultado = backtrack(csp, inference=True,variable_heuristic = MOST_CONSTRAINED_VARIABLE)
        if heuristica_variable == "HIGHEST_DEGREE_VARIABLE" and heuristica_valor == "None":
            resultado = backtrack(csp, inference=False,variable_heuristic=HIGHEST_DEGREE_VARIABLE)
        if heuristica_variable == "None" and heuristica_valor=="LEAST_CONSTRAINING_VALUE":
            resultado = backtrack(csp, inference=False,value_heuristic=LEAST_CONSTRAINING_VALUE)
        if heuristica_variable == "MOST_CONSTRAINED_VARIABLE" and heuristica_valor == "LEAST_CONSTRAINING_VALUE":
            resultado = backtrack(csp, inference=False,variable_heuristic=MOST_CONSTRAINED_VARIABLE,value_heuristic=LEAST_CONSTRAINING_VALUE)
        if heuristica_variable == "HIGHEST_DEGREE_VARIABLE" and heuristica_valor == "LEAST_CONSTRAINING_VALUE":
            resultado = backtrack(csp, inference=False,variable_heuristic=HIGHEST_DEGREE_VARIABLE,value_heuristic=LEAST_CONSTRAINING_VALUE)
    else:
<<<<<<< HEAD
            resultado= min_conflicts(csp,initial_assignment=None, iterations_limit=1000)
    
    
    #return resultado 

    posiciones=resultado
    tablero=[["#" for i in range(8)] for i in range(8)]
    for i in posiciones:
        tablero[posiciones[i][0]][posiciones[i][1]]=i
    for i in tablero:
        print i
        print
    return resultado 
=======
        resultado= min_conflicts(csp,initial_assignment=None, iterations_limit=1000)


    return resultado

    #posiciones=resultado
    #tablero=[["#" for i in range(8)] for i in range(8)]
    #for i in posiciones:
        #tablero[posiciones[i][0]][posiciones[i][1]]=i
    #for i in tablero:
        #print i
        #print
    #return resultado
>>>>>>> 151d29829e8ff42b1a0aed06a39c7472378d7ac9
