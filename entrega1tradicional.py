import math
import ast
from simpleai.search import SearchProblem
from simpleai.search.traditional import breadth_first, depth_first, limited_depth_first, iterative_limited_depth_first, uniform_cost, greedy, astar
from simpleai.search.viewers import WebViewer, ConsoleViewer,BaseViewer


next = {"san francisco":["chicago","los angeles","tokyo","manila"],
"los angeles":["chicago","mexico city","san francisco","sydney"],
"chicago":["los angeles","san francisco","mexico city","atlanta","montreal"],
"mexico city":["chicago","los angeles","miami","lima","bogota"],
"miami":["atlanta","mexico city","washington","bogota"],
"atlanta":["chicago","miami","washington"],
"washington":["miami","atlanta","montreal","new york"],
"new york":["montreal","washington","madrid","london"],
"bogota":["miami","mexico city","lima","sao paulo","buenos aires"],
"lima":["santiago","mexico city","bogota"],
"santiago":["lima"],
"buenos aires":["bogota","sao paulo"],
"sao paulo":["buenos aires","bogota","lagos","madrid"],
"lagos":["sao paulo","kinshasa","khartoum"],
"kinshasa":["lagos","khartoum","johannesburg"],
"khartoum":["lagos","kinshasa","johannesburg","cairo"],
"johannesburg":["kinshasa","khartoum"],
"cairo":["algiers","istanbul","baghdad","riyadh"],
"algiers":["cairo","madrid","paris","istanbul",],
"riyadh":["cairo","baghdad","karachi"],
"istanbul":["milan","algiers","cairo","baghdad","moscow","st petersburg"],
"baghdad":["istanbul","cairo","riyadh","karachi","tehran"],
"karachi": ["baghdad","riyadh","tehran","delhi","mumbai"],
"moscow":["st petersburg","istanbul","tehran"],
"tehran": ["moscow","baghdad","karachi","delhi"],
"delhi":["tehran","karachi","mumbai","chennai","kolkata"],
"mumbai":["karachi","delhi","chennai"],
"chennai":["mumbai","delhi","kolkata","bangkok","jakarta"],
"kolkata":["delhi","chennai","bangkok","hong kong"],
"bangkok":["kolkata","chennai","jakarta","ho chi minh city","hong kong"],
"ho chi minh city":["bangkok","jakarta","manila","hong kong"],
"manila":["sydney","ho chi minh city","taipei","hong kong","san francisco"],
"sydney":["jakarta","manila","los angeles"],
"jakarta":["chennai","bangkok","ho chi minh city","sydney"],
"hong kong":["kolkata","bangkok","ho chi minh city","manila","taipei","shanghai"],
"taipei":["hong kong","osaka","shanghai","manila"],
"osaka":["taipei","tokyo"],
"shanghai":["hong kong","taipei","tokyo","seoul","beijing"],
"tokyo":["seoul","osaka","san francisco"],
"beijing":["seoul","shanghai"],
"seoul":["beijing","shanghai","tokyo"],
"london":["new york","madrid","paris","essen"],
"madrid":["london","paris","algiers","new york","sao paulo"],
"paris":["london","madrid","algiers","milan","essen"],
"milan":["paris","istanbul","essen"],
"essen":["london","paris","milan","st petersburg"],
"st petersburg":["essen","istanbul","moscow"],
"montreal":["washington","new york","chicago"]
}



#los angeles 1,tokyo 3,seoul 2

class pandemicProblem(SearchProblem):

    def is_goal(self,state):
        suma = 0
        estado = state.split('-')
        diccionario=ast.literal_eval(estado[1])
        valores=diccionario.values()
        for valor in valores:
            suma= suma + valor

        return suma==0

    def cost(self,state1,action,state2):
        return  1



    def actions (self,state):
        estado=state.split("-")
        diccionario=ast.literal_eval(estado[1])
        acciones=[]
        if (diccionario.has_key(estado[0])) and (diccionario[estado[0]]>0) :

#          if diccionario[estado[0]]>0:
            #acciones="-".join([estado[0],str(diccioario),"curar"])
            acciones=["curar"]
        else:
            lista=next[estado[0]]
            for sucesor in lista:
                acciones.append(sucesor)
        return acciones



    def result(self,state,action):
        estado=state.split("-")
        diccionario=ast.literal_eval(estado[1])
        if action=="curar":
            diccionario[estado[0]]-=1
            estado="-".join([estado[0],str(diccionario)])
        else:
            estado="-".join([action,str(diccionario)])
        return estado

    def heuristic(self,state):
        estado=state.split("-")
        diccionario=ast.literal_eval(estado[1])
        valores=diccionario.values()
        claves=diccionario.keys()
        cantidad_ciudades=0
        cantidad_virus=0
        for ciudad in claves:
            if diccionario[ciudad]>0:
                cantidad_ciudades+=1
            cantidad_virus=cantidad_virus+ diccionario[ciudad]

        return cantidad_virus+cantidad_ciudades




def resolver(metodo_busqueda,ciudad,infecciones):

    estado="-".join([ciudad,str(infecciones)])
    visor =BaseViewer()
    if metodo_busqueda == "astar":
        result = astar(pandemicProblem(estado),viewer=visor)

    if metodo_busqueda == "breadth_first":
        result = breadth_first(pandemicProblem(estado),graph_search=True,viewer=visor)

    if metodo_busqueda == "depth_first":
        result = depth_first(pandemicProblem(estado),graph_search=True,viewer=visor)

    if metodo_busqueda =="greedy":
        result = greedy(pandemicProblem(estado),graph_search=True,viewer=visor)

    if metodo_busqueda =="limited_depth_first":
        result = limited_depth_first(pandemicProblem(estado),10, graph_search= False , viewer=visor)


    return result
    #print visor.stats
    #print result.path()
    #print len(result.path())





