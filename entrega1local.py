import random
import math
from simpleai.search import SearchProblem,hill_climbing, beam, hill_climbing_random_restarts
from simpleai.search.viewers import ConsoleViewer,BaseViewer,WebViewer

CORDENADAS={"san francisco":["101", "295"],
"los angeles" :["118","178"],
"mexico city":["194","406"],
"chicago":["206","264"],
"atlanta":["237","327"],
"montreal":["289", "262"],
"miami":["290", "393"],
"washington":["325", "322"],
"new york":["353","271"],
"bogota":["284", "479"],
"lima":["254", "567"],
"santiago":["264", "660"],
"buenos aires":["351", "642"],
"sao paulo" :["400","581"],
"london" :["506","223"],
"essen": ["589","207"],
"st petersburg": ["681","191"],
"madrid":["495","304"],
"paris": ["569","263"],
"milan": ["624","247"],
"algiers" :["587","350"],
"istanbul":["660","296"],
"moscow":["726","247"],
"cairo":["648","366"],
"baghdad":["720","338"],
"tehran":["782","286"],
"riyadh":["729","414"],
"karachi":["798","365"],
"delhi":["859","342"],
"mumbai":["806","427"],
"kolkata":["917","362"],
"chennai":["870","472"],
"beijing":["964","267"],
"seoul":["1038","263"],
"shanghai":["969","325"],
"tokyo":["1095","296"],
"hong kong":["976","395"],
"taipei":["1043","383"],
"osaka":["1102","359"],
"bangkok":["928","432"],
"ho chi minh city":["980","491"],
"manila":["1061","486"],
"jakarta":["929","542"],
"lagos":["560","463"],
"khartoum":["665","448"],
"kinshasa":["612","522"],
"johannesburg":["660","605"],
"sydney":["1109","656"]}

next = {"san francisco":["chicago","los angeles"],
"los angeles":["chicago","mexico city","san francisco"],
"chicago":["los angeles","san francisco","mexico city","atlanta","montreal"],
"mexico city":["chicago","los angeles","miami","lima","bogota"],
"miami":["atlanta","mexico city","washington","bogota"],
"atlanta":["chicago","miami","washington"],
"washington":["miami","atlanta","montreal","new york"],
"new york":["montreal","washington","madrid","london"],
"bogota":["miami","mexico city","lima","sao paulo","buenos aires"],
"lima":["santiago","mexico city","bogota"],
"santiago":["lima"],
"buenos aires":["bogota","sao paulo"],
"sao paulo":["buenos aires","bogota","lagos","madrid"],
"lagos":["sao paulo","kinshasa","khartoum"],
"kinshasa":["lagos","khartoum","johannesburg"],
"khartoum":["lagos","kinshasa","johannesburg","cairo"],
"johannesburg":["kinshasa","khartoum"],
"cairo":["algiers","istanbul","baghdad","riyadh"],
"algiers":["cairo","madrid","paris","istanbul",],
"riyadh":["cairo","baghdad","karachi"],
"istanbul":["milan","algiers","cairo","baghdad","moscow","st petersburg"],
"baghdad":["istanbul","cairo","riyadh","karachi","tehran"],
"karachi": ["baghdad","riyadh","tehran","delhi","mumbai"],
"moscow":["st petersburg","istanbul","tehran"],
"tehran": ["moscow","baghdad","karachi","delhi"],
"delhi":["tehran","karachi","mumbai","chennai","kolkata"],
"mumbai":["karachi","delhi","chennai"],
"chennai":["mumbai","delhi","kolkata","bangkok","jakarta"],
"kolkata":["delhi","chennai","bangkok","hong kong"],
"bangkok":["kolkata","chennai","jakarta","ho chi minh city","hong kong"],
"ho chi minh city":["bangkok","jakarta","manila","hong kong"],
"manila":["sydney","ho chi minh city","taipei","hong kong"],
"sydney":["jakarta","manila"],
"jakarta":["chennai","bangkok","ho chi minh city","sydney"],
"hong kong":["kolkata","bangkok","ho chi minh city","manila","taipei","shanghai"],
"taipei":["hong kong","osaka","shanghai","manila"],
"osaka":["taipei","tokyo"],
"shanghai":["hong kong","taipei","tokyo","seoul","beijing"],
"tokyo":["seoul","osaka"],
"beijing":["seoul","shanghai"],
"seoul":["beijing","shanghai","tokyo"],
"london":["new york","madrid","paris","essen"],
"madrid":["london","paris","algiers","new york","sao paulo"],
"paris":["london","madrid","algiers","milan","essen"],
"milan":["paris","istanbul","essen"],
"essen":["london","paris","milan","st petersburg"],
"st petersburg":["essen","istanbul","moscow"],
"montreal":["washington","new york","chicago"]
}

INFECCIONES = {
"curar":0,
"san francisco":0,
"los angeles":0,
"chicago":0,
"mexico city":0,
"miami":0,
"atlanta":0,
"washington":0,
"new york":0,
"bogota":0,
"lima":0,
"santiago":0,
"buenos aires":0,
"sao paulo":0,
"lagos":0,
"kinshasa":0,
"khartoum":0,
"johannesburg":0,
"cairo":0,
"algiers":0,
"riyadh":0,
"istanbul":0,
"baghdad":0,
"karachi":0,
"moscow":0,
"tehran":0,
"delhi":0,
"mumbai":0,
"chennai":0,
"kolkata":0,
"bangkok":0,
"ho chi minh city":0,
"manila":0,
"sydney":0,
"jakarta":0,
"hong kong":0,
"taipei":0,
"osaka":0,
"shanghai":0,
"tokyo":0,
"beijing":0,
"seoul":0,
"london":0,
"madrid":0,
"paris":0,
"milan":0,
"essen":0,
"st petersburg":0,
"montreal":0,
}

CIUDADES=next.keys()


class problemaLocal(SearchProblem):

	def actions(self,state):
		acciones=[]
		lista=next[state]
		for sucesor in lista:
			acciones.append(sucesor)
		return acciones



	def result(self,state,action):
		#print "estado"
		#print state
		return action


	def value(self,state):
		distanciaTotal=0
		for ciudad in ciudadesInfectadas:
			distancia_x=abs(int(CORDENADAS[state][0])-int(CORDENADAS[ciudad][0]))
			distancia_y= abs(int(CORDENADAS[state][1])-int(CORDENADAS[ciudad][1]))
			distanciaTotal += (math.sqrt(distancia_x**2 + distancia_y**2)) * INFECCIONES[ciudad]

		return -distanciaTotal


	def generate_random_state(self):#Selecciona una ciudad al azar

		ciudad = CIUDADES[random.randint(0,len(CIUDADES)-1)]

		return str(ciudad)


def resolver(metodo_busqueda,infecciones):
	INFECCIONES.update(infecciones)
	listaAux=INFECCIONES.items()
	global ciudadesInfectadas
	ciudadesInfectadas=[]
	for tupla in listaAux:
		if tupla[1]>0:
			ciudadesInfectadas.append(tupla[0])
	#print ciudadesInfectadas
	visor= BaseViewer();
	#visor= WebViewer();
	if metodo_busqueda=="hill_climbing":
		result= hill_climbing(problemaLocal("tokyo"),100,visor)
	if metodo_busqueda=="hill_climbing_random_restarts":
		result=hill_climbing_random_restarts(problemaLocal(),100,1000,viewer=visor)
	if metodo_busqueda=="beam":
		result=beam(problemaLocal(),100,100,viewer=visor)

	return result
	#print result.path()
	#print problemaLocal().value(result.state)
	#print visor.stats

